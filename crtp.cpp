#include <vector>

struct Node : public std::vector<Node> {

  // Add a child node to the current node
  Node &addNode() { return emplace_back(Node()); }
};

int main() {

  // Create top node
  Node node;

  // Add sub tasks
  node.addNode();
  node.addNode();
  node.addNode();

  // Add a sub-sub-task
  auto n1 = node.addNode();
  n1.addNode();
}
