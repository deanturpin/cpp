#include <iostream>

void func(int x, int y, int z) {

  std::cout << x << "\n";
  std::cout << y << "\n";
  std::cout << z << "\n";
}

int main() {

  int i = 0;
  func(++i, ++i, ++i);
}
