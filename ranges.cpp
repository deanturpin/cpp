#include <iostream>
#include <ranges>
#include <vector>

int main() {

  const std::vector<int> vect{1, 2, 3, 4, 5, 6, 7};
  const auto func = [](const auto i) { return i % 2; };
  const auto incr = [](const auto i) { return i + 1; };

  for (const auto &i :
       vect | std::views::filter(func) | std::views::transform(incr))
    std::cout << i << "\n";
}
