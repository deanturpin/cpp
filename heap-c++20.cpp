#include <iostream>
#include <iterator>
#include <vector>

int main() {

  std::cout << "vector\n";
  std::vector<int> v{1, 6, 2, 7, 9, 3, 54, 456, 345, 5, 5};
  std::copy(std::cbegin(v), std::cend(v),
            std::ostream_iterator<int>(std::cout, "\n"));

  std::cout << "heap\n";
  std::make_heap(std::begin(v), std::end(v));
  std::copy(std::cbegin(v), std::cend(v),
            std::ostream_iterator<int>(std::cout, "\n"));
}
