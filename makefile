objects = $(patsubst %.cpp, tmp/%.o, $(wildcard *.cpp))

all: tmp
	$(CXX) --version
	@echo $(shell nproc) threads available
	@echo > output.md
	$(MAKE) -j $(shell nproc) $(objects)

CXX ?= g++-10

apt:
	apt update
	apt install --yes libtbb-dev

# --pedantic-errors

CXXFLAGS ?= --std=c++2a --all-warnings --extra-warnings \
	-Wshadow -Wfloat-equal -Weffc++ -Wdelete-non-virtual-dtor \
	-Warray-bounds -Wdeprecated-copy \
	-Og

	# GCC only
	# -Wattribute-alias -Wformat-overflow -Wformat-truncation -Wclass-conversion
	# -Wmissing-attributes -Wstringop-truncation

tmp/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ $< -lpthread -ltbb
	@echo '```' >> output.md
	./$@
	@echo '```' >> output.md

tmp:
	mkdir -p $@

clean:
	rm -rf tmp *.bin *.out
