#include <algorithm>
#include <cmath>
#include <execution>
#include <iostream>
#include <vector>

int main() {

  std::vector<double> vec(100'000'000);
  std::iota(vec.begin(), vec.end(), 0);

  using std::sqrt;

  std::for_each(std::execution::par, vec.begin(), vec.end(), [](auto &i) {
    i = sqrt(sqrt(sqrt(sqrt(sqrt(sqrt(sqrt(sqrt(i))))))));
  });

  std::for_each(vec.cbegin(), std::next(vec.cbegin(), 10),
                [](const auto &i) { std::cout << i << "\n"; });
}
